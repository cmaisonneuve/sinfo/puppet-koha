# This class handles configuration of the repository and installation
# of the koha-common package.
#
# @private
#
class koha::install {

  assert_private()

  # Configure package repository
  # (requires the puppetlabs/apt module)
  if $koha::manage_repository {
    apt::source { 'koha':
      comment  => 'Koha Debian package repository',
      location => $koha::repo_url,
      release  => $koha::repo_suite,
      repos    => 'main',
      pin      => $koha::repo_pin_priority,
      key      => {
        'id'     => '3EA44636DBCE457DA2CF8D823C9356BBA2E41F10',
        'server' => $koha::repo_keyserver_url,
      },
      include  => {
        'src' => false,
        'deb' => true,
      },
      before   => Package['koha-common'],
    }

    Package['koha-common'] {
      require => Class['apt::update']
    }
  }

  # Install main package
  package { 'koha-common':
    ensure => $koha::ensure_koha_version
  }

  # Install translations
  if count($koha::translations) > 0 {
    $koha::translations.each | $lang | {
      exec { "install ${lang} Koha translation":
        command     => "/usr/sbin/koha-translate --install ${lang}",
        creates     => "${koha::homedir}/opac/htdocs/opac-tmpl/bootstrap/${lang}",
        refreshonly => true,
        require     => Package['koha-common']
      }
    }
  }

}
