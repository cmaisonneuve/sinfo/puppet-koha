# This class handles Koha configuration files.
#
# @private
#
class koha::config {

  assert_private()

  # Deploy Koha configuration files
  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0644';
    '/etc/default/koha-common':
      content => epp('koha/koha-common.epp');
    "${koha::etcdir}/koha-common.conf":
      content => epp('koha/koha-common.conf.epp');
    "${koha::etcdir}/koha-sites.conf":
      content => epp('koha/koha-sites.conf.epp');
  }

  # Deploy Apache shared configuration files
  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0644',
      tag   => 'restart-apache';
    "${koha::etcdir}/apache-shared.conf":
      source => $koha::apache_shared_source;
    "${koha::etcdir}/apache-shared-disable.conf":
      source => $koha::apache_shared_disable_source;
    "${koha::etcdir}/apache-shared-intranet.conf":
      source => $koha::apache_shared_intranet_source;
    "${koha::etcdir}/apache-shared-intranet-plack.conf":
      source => $koha::apache_shared_intranet_plack_source;
    "${koha::etcdir}/apache-shared-opac.conf":
      source => $koha::apache_shared_opac_source;
    "${koha::etcdir}/apache-shared-opac-plack.conf":
      source => $koha::apache_shared_opac_plack_source;
  }

  if $koha::manage_apache {
    File <| tag == 'restart-apache' |> {
      notify => Service['apache2']
    }
  }

  # Deploy templates
  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0644';
    "${koha::etcdir}/apache-site.conf.in":
      source => $koha::apache_site_template_source;
    "${koha::etcdir}/apache-site-https.conf.in":
      source => $koha::apache_site_https_template_source;
    "${koha::etcdir}/koha-conf-site.xml.in":
      source => $koha::site_conf_template_source;
  }

  # Deploy default SQL data
  if $koha::manage_defaultsql {
    file {
      default:
        owner => 'root',
        group => 'root',
        mode  => '0640';
      "${koha::etcdir}/sql":
        ensure => directory,
        mode   => '0750';
      "${koha::etcdir}/sql/default.sql.gz":
        ensure => present,
        source => $koha::defaultsql_sourcegz,
    }
  }

  # Configure Apache
  # (requires the puppetlabs/apache module)
  if $koha::manage_apache {

    class { 'apache':
      mpm_module      => 'itk',
      purge_vhost_dir => false,
      default_mods    => false,
    }

    include apache::mod::rewrite
    include apache::mod::cgi
    include apache::mod::deflate
    include apache::mod::expires
    include apache::mod::headers
    include apache::mod::env
    include apache::mod::setenvif
    include apache::mod::mime
    include apache::mod::dir

    # Ensure Apache listens on OPAC and Intra ports
    unless empty($koha::opacport) {
      ensure_resource('apache::listen', $koha::opacport)
    }
    unless empty($koha::intraport) {
      ensure_resource('apache::listen', $koha::intraport)
    }

  }

}
