# Manage configured instances
#
# @private
#
class koha::instances {

  assert_private()

  if count($koha::instances) > 0 {
    create_resources('koha::instance', $koha::instances, $koha::default_instance_params)
  }

}
