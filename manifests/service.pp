# Manage the koha-common service.
#
# @private
#
class koha::service {

  assert_private()

  if $koha::manage_service {

    service { 'koha-common':
      ensure     => $koha::ensure_service,
      enable     => $koha::enable_service,
      hasrestart => true,
      hasstatus  => true,
      subscribe  => File['/etc/default/koha-common'],
    }

  }
}
