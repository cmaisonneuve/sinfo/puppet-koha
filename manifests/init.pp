# koha
#
# This is the main class, which contains all other classes in this module.
#
class koha (
  Stdlib::Unixpath $etcdir,
  Stdlib::Unixpath $homedir,
  Stdlib::Unixpath $libdir,
  Stdlib::Unixpath $vardir,
  Boolean $manage_repository,
  Stdlib::Httpurl $repo_url,
  String $repo_suite,
  Integer $repo_pin_priority,
  String $repo_keyserver_url,
  Boolean $manage_packages,
  String $ensure_koha_version,
  Boolean $manage_service,
  Stdlib::Ensure::Service $ensure_service,
  Boolean $enable_service,
  Boolean $auto_translations_update,
  Optional[Array] $translations,
  String $domain,
  Integer $intraport,
  Integer $opacport,
  String $intraprefix,
  String $intrasuffix,
  String $opacprefix,
  String $opacsuffix,
  String $defaultsql,
  Enum['marc21', 'nomarc', 'unimarc'] $zebra_marc_format,
  String[2, 2] $zebra_language,
  Enum['dom', 'grs1'] $biblios_indexing_mode,
  Enum['dom', 'grs1'] $authorities_indexing_mode,
  Boolean $use_memcached,
  Array $memcached_servers,
  String $memcached_prefix,
  Boolean $use_indexer_daemon,
  Optional[String] $alternate_indexer_daemon,
  Integer[1] $indexer_timeout,
  Optional[String] $indexer_params,
  Boolean $manage_defaultsql,
  Optional[String] $defaultsql_sourcegz,
  Boolean $manage_apache,
  Optional[Hash] $instances,
  Optional[Hash] $default_instance_params,
  String $apache_shared_source,
  String $apache_shared_disable_source,
  String $apache_shared_intranet_source,
  String $apache_shared_intranet_plack_source,
  String $apache_shared_opac_source,
  String $apache_shared_opac_plack_source,
  String $apache_site_template_source,
  String $apache_site_https_template_source,
  String $site_conf_template_source,
) {

  contain koha::install
  contain koha::config
  contain koha::service
  contain koha::instances

  Class['koha::install']
  -> Class['koha::config']
  -> Class['koha::service']
  -> Class['koha::instances']

}
