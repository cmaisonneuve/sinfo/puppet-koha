# koha

#### Table of Contents

1. [Description](#description)
2. [Setup](#setup)
    * [Setup requirements](#setup-requirements)
    * [Getting started](#getting-started)
3. [Limitations](#limitations)
4. [Development](#development)

## Description

This module can be used to manage Koha, the integrated library management system.

It configures the Koha project APT repository, installs the base package (`koha-common`), deploys configuration files into `/etc/koha` from templates and manages the `koha-common` service. The `koha::instance` defined type can be used to deploy one or more instances.

Optionally, it can install and configure the Apache webserver, as well as manage its service daemon.

## Setup

### Setup requirements

This module depends on the [puppetlabs-stdlib](https://forge.puppet.com/puppetlabs/stdlib) module as well as the [PuppetLabs APT module](https://forge.puppet.com/puppetlabs/apt) (unless `$manage_repository` is set to `false`).

If the `$manage_apache` parameter is set to `true`, the module will additionally require the [PuppetLabs Apache module](https://forge.puppet.com/puppetlabs/apache).

### Getting started

The example below will configure the Koha APT repository, install the `koha-common` package and deploy default configuration settings.

``` puppet
class { 'koha':
  domain => '.example.com'
}
```

To create a Koha instance, the `koha::instance` defined type may be used as below. It will deploy an instance at the `koha.example.com` URL.


``` puppet
koha::instance { 'koha':
  opacport => 80,
  intraport => 8080,
}
```

## Limitations

* Only the latest versions of Debian and Ubuntu are supported.
* The module doesn't (yet) manage certain aspects of Koha, such as Zebra configuration files and cron jobs.
* The module doesn't (yet) have any unit or integration tests.

## Development

This module's development is tracked on GitLab. Please submit issues and merge requests on the [cmaisonneuve/puppet-koha](https://gitlab.com/cmaisonneuve/puppet-koha/) project page.

Upstream Koha development happens at [koha-community.org](https://koha-community.org/).
