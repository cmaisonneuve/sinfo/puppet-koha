Facter.add(:koha) do
  confine { Facter.value("os")["family"] == "Debian" }
  setcode do
    koha = {}
    koha['version'] = Facter::Core::Execution.exec("perl -I/usr/share/koha/lib -e 'eval \"use Koha; print Koha::version;\" or exit 0'")
    koha
  end
end
